import state from "./rootState.js";
import fbinstance  from "../config/firebase.js";
import firebase from "firebase";
import pbkdf2 from "pbkdf2";

const firestore = firebase.firestore();
require("firebase/firestore");

export default {
	avatarURL: () => state.isGuest ? require("~/static/img/detective.png") : state.avatarURL,
	displayName: () => state.displayName,
	encryptionKeyphrase: () => state.encryptionKeyphrase,
	encryptionKey: () => {
		return !state.encryptionKeyphrase ? undefined : pbkdf2.pbkdf2Sync(state.encryptionKeyphrase, "qsB9E7zxQXMQ39ASu4DsEA5STHS6GcEhV", 1000, 32, "sha512");
	},
	fbinstance: () => fbinstance,
	firestore: () => firestore,
	googleProvider: () => new firebase.auth.GoogleAuthProvider(),
	isGuest: () => state.isGuest,
	userId: () => state.userId,
}
