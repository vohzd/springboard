export default {
	avatarURL: null,
	displayName: "Guest",
	encryptionKeyphrase: null,
	encryptionKey: null,
	isGuest: true,
	userId: null,
};
