import state from "./rootState.js";

export default {
	SET_AVATAR(state, avatarURL){
		state.avatarURL = avatarURL;
	},
	SET_DISPLAY_NAME(state, name){
		state.displayName = name;
	},
	SET_ENCRYPTION_KEYPHRASE(state, keyphrase){
		state.encryptionKeyphrase = keyphrase;
	},
	SET_IS_GUEST(state, bool){
		state.isGuest = bool;
	},
	SET_USER_ID(state, id){
		state.userId = id;
	},
	SET_LOCAL_STATE(state, meta){
		state[meta.stateKey] = meta.data;
	}
};
