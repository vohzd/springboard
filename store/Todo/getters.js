import state from "./state.js";

export default {
  isTodoModalShown: () => state.isTodoModalShown,
  todoMeta: () => state.todoMeta,
};
