import state from "./state.js";

export default {
  SET_IS_TODO_MODAL_SHOWN(state, bool){
    state.isTodoModalShown = bool;
  },
  SET_TODO_META(state, payload){
    state.todoMeta = payload;
  }
}
