import getters 				  						from "./rootGetters.js";
import state 				    						from "./rootState.js";

export default {
	/* AUTH STUFF */
	login({ commit, dispatch }){
		this.getters.fbinstance.auth().signInWithPopup(this.getters.googleProvider).then((res) => {
			commit("SET_DISPLAY_NAME", res.user.displayName);
			commit("SET_AVATAR", res.user.photoURL);
			commit("SET_IS_GUEST", false);
		}).catch((err) => console.log(err));
	},
	logout({ commit }){
		this.getters.fbinstance.auth().signOut().then(() => {
			commit("SET_DISPLAY_NAME", "Guest");
			commit("SET_IS_GUEST", true);
		}).catch((err) => console.log(err));
	},
  determineSignInState({ commit, dispatch }){
    this.getters.fbinstance.auth().onAuthStateChanged((user) => {
      if (user === null) {
				console.log("no user");
      }
			else {
				commit("SET_USER_ID", user.uid);
				commit("SET_DISPLAY_NAME", user.displayName);
				commit("SET_AVATAR", user.photoURL);
				commit("SET_IS_GUEST", false);
				dispatch("getEncryptionKeyphrase");
				// NOT NEEDED FOR NOW
				//dispatch("getProfile");
			}
    });
  },
	getProfile({ commit, dispatch }){
		dispatch("readDB", {
			"dbName": "ORGANISER"
		});
	},
	getEncryptionKeyphrase({ commit }){
		console.log("running this...")
		let ls = localStorage.getItem("springboard_e");
		console.log(JSON.parse(ls));
		if (ls){
			console.log("fires...")
			commit("SET_ENCRYPTION_KEYPHRASE", ls ? JSON.parse(ls)[this.getters.userId] :  null);
		}
	},
	hideModal({ commit, dispatch }, modalName){
		console.log("hide me pls...");
		commit(`SET_IS_${modalName}_MODAL_SHOWN`, false);
	},
	showModal({ commit, dispatch }, modalName){
		commit(`SET_IS_${modalName}_MODAL_SHOWN`, true);
	},
	/* UNUSED FOR NOW
	getFromDB({ commit }, dbKey){
		let res = undefined;
		this.getters.firestore.collection(dbKey).where("email", "==", this.getters.profileMeta.email).get().then((res) => {
			console.log(res)
        res.forEach((doc) => {
					res = doc.data()
        });
    });
		return res;
  }, */
	/* BASIC CRUD HERE LADS */
	async createDB({ commit }, payload){
		await this.getters.fbinstance.firestore().collection(payload.dbName).doc(this.getters.userId).set(payload.data);
		/*
		commit("SET_LOCAL_STATE", {
			"stateKey": payload.dbName,
			"data": payload.data
		})*/
	},
	async readDB({ commit }, dbName){
		let doc = await this.getters.fbinstance.firestore().collection(dbName).doc(this.getters.userId).get();
		if (doc.exists) {
			this.getters.fbinstance.firestore().collection(dbName).doc(this.getters.userId).onSnapshot((data) => {
				commit(`SET_${dbName}_META`, data.data());
			});
		};
	},
	async updateDB({ commit, dispatch }, payload){
		console.log("HALLO FROM THE ACTIONS");
		console.log(payload);
		let doc = await this.getters.fbinstance.firestore().collection(payload.dbName).doc(this.getters.userId).get();
		doc.exists ? await this.getters.fbinstance.firestore().collection(payload.dbName).doc(this.getters.userId).update(payload.data) : await dispatch("createDB", payload);
		console.log(doc.data());
		/*
		commit("SET_LOCAL_STATE", {
			"stateKey": payload.dbName,
			"data": payload.data
		})*/
	},/*
	deleteDB({ commit }, payload){

	}*/
};
