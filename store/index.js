/*
 * LIBRARIES
 */
import Vue 										from "vue";
import Vuex 									from "vuex";

/*
 * TOP LEVEL VUEX STUFF
 */
import state 									from "./rootState.js";
import myMutations 						from "./rootMutations.js";
import actions 								from "./rootActions.js";
import getters 								from "./rootGetters.js";

/*
 * INDIVIDUAL COMPONENT VUEX STUFF
 */

//import Chat 						  		from "./modules/Chat/index.js";
//import Feed 									from "./modules/Feed/index.js";
//import Organiser 						  	from "./Organiser/index.js";
import Todo 										from "./Todo/index.js";

Vue.use(Vuex);

export default () => {
	return new Vuex.Store({
		actions,
		getters,
		modules: {
			//Organiser,
			Todo
		},
		mutations: {
			...myMutations
		},
		state
	});

};
