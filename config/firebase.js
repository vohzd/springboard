import firebase from "firebase";

const config = {
	apiKey: "AIzaSyBS4vXeYYVZaSV1MV619aMwqz7kb_LOl2o",
	authDomain: "springboardv2.firebaseapp.com",
	databaseURL: "https://springboardv2.firebaseio.com",
	projectId: "springboardv2",
	storageBucket: "springboardv2.appspot.com",
	messagingSenderId: "739178322795"
}

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
