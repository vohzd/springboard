# Springboard
My personal dashboard á la iGoogle, but in realtime!

### V2 Major Refactor

- Now uses `nuxtjs` which hides a lot of the boring implementations and is just nicer to work with.
- Todo now supports encryption.
- Latest version of everything.
- Style overhaul
- Less buggy
- More Front-end functionality coming soon

https://vohzd.com/springboard
