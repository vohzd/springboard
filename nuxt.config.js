module.exports = {
  head: {
    title: "Springboard",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Springboard, organise your life" }
    ]
  },
  loading: { color: "#b56d82" },
  modules: [
    [ "@nuxtjs/google-analytics", { id: "UA-104309082-3" } ]
  ],
  router: {
    //base: "/"
    base: "/springboard"
  },
  plugins: [
    "~/plugins/modal.js"
  ],
  mode: "spa"
}
